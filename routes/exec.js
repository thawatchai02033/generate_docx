var express = require('express');
var router = express.Router();

var PizZip = require('pizzip');
var Docxtemplater = require('docxtemplater');
var ImageModule = require('open-docxtemplater-image-module');

var fs = require('fs');
var path = require('path');

const Downloader = require('nodejs-file-downloader');
var mammoth = require("mammoth");

//Below the options that will be passed to ImageModule instance
var opts = {}
opts.centered = false; //Set to true to always center images
opts.fileType = "docx"; //Or pptx

//Pass your image loader
opts.getImage = function(tagValue, tagName) {
    //tagValue is 'examples/image.png'
    //tagName is 'image'
    return fs.readFileSync(tagValue);
}

//Pass the function that return image size
opts.getSize = function(img, tagValue, tagName) {
    //img is the image returned by opts.getImage()
    //tagValue is 'examples/image.png'
    //tagName is 'image'
    //tip: you can use node module 'image-size' here
    return [150, 150];
}

var imageModule = new ImageModule(opts);

// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function(error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

/* GET users listing. */
router.get('/', function(req, res, next) {
    //Load the docx file as a binary
    var content = fs
        .readFileSync(path.resolve(__dirname, 'input.docx'), 'binary');
    var zip = new PizZip(content);
    var doc;
    try {
        // doc = new Docxtemplater(zip);
        doc = new Docxtemplater()
            .attachModule(imageModule)
            .loadZip(zip)
            .setData({
                first_name: 'John',
                last_name: 'Doe',
                phone: '0652455478',
                description: 'New Website',
                image: path.resolve(__dirname, 'OIP.jpg')
            })
            .render();
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }
    // try {
    //   // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    //   doc.render()
    // }
    // catch (error) {
    //   // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    //   errorHandler(error);
    // }

    var buf = doc.getZip()
        .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
    fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);

    res.send('respond with a resource');
});

router.post('/docx', function(req, res, next) {
    // console.log(req.params.data)
    // res.set({
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Methods': 'DELETE,GET,PATCH,POST,PUT',
    //     'Access-Control-Allow-Headers': 'Content-Type,Authorization'
    // });
    //Load the docx file as a binary
    console.log(req.body.data)
    var content = fs
        .readFileSync(path.resolve(__dirname, '../fileExec/TemplateProject.docx'), 'binary');
    var zip = new PizZip(content);
    var doc;
    try {
        // doc = new Docxtemplater(zip);
        doc = new Docxtemplater()
            .loadZip(zip)
            .setData(req.body.data)
            .render();

        var buf = doc.getZip()
            .generate({type: 'nodebuffer'});

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        fs.writeFileSync(path.resolve(__dirname, '../fileExec/' + req.body.data.projectcode + '.docx'), buf);
        res.send('Success');
    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        res.send('Error');
        errorHandler(error);
    }
    // try {
    //   // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    //   doc.render()
    // }
    // catch (error) {
    //   // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    //   errorHandler(error);
    // }
})

router.get('/test', function(req, res, next) {
    // res.download(path.resolve(__dirname, '../fileExec/TemplateProject.docx'))
    var options = {
        styleMap: [
            "p[style-name='Section Title'] => h1:fresh",
            "p[style-name='Subsection Title'] => h2:fresh"
        ]
    };
    mammoth.convertToHtml({path: path.resolve(__dirname, '../fileExec/TemplateProject.docx')}, options)
        .then(function(result){
            var html = result.value; // The generated HTML
            var messages = result.messages; // Any messages, such as warnings during conversion
            console.log(html)
            res.send(html)
        })
        .done();
    // res.sendFile(path.resolve(__dirname, '../fileExec/TemplateProject.docx'))
})

    module.exports = router;
