var express = require('express');
var router = express.Router();

var PizZip = require('pizzip');
var Docxtemplater = require('docxtemplater');
var ImageModule = require('open-docxtemplater-image-module');
const imageToBase64 = require('image-to-base64');

var fs = require('fs');
var path = require('path');

//Below the options that will be passed to ImageModule instance
var opts = {}
opts.centered = false; //Set to true to always center images
opts.fileType = "docx"; //Or pptx

function base64DataURLToArrayBuffer(dataURL) {
    const base64Regex = /^data:image\/(png|jpg|svg|svg\+xml);base64,/;
    if (!base64Regex.test(dataURL)) {
        return false;
    }
    const stringBase64 = dataURL.replace(base64Regex, "");
    let binaryString;
    if (typeof window !== "undefined") {
        binaryString = window.atob(stringBase64);
    } else {
        binaryString = new Buffer(stringBase64, "base64").toString("binary");
    }
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        const ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes.buffer;
}
const imageOpts = {
    getImage(tag) {
        return base64DataURLToArrayBuffer(tag);
    },
    getSize() {
        return [100, 100];
    },
};
// var imageModule = new ImageModule(opts);

// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function(error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

function ImageToBase64Func(ImageUrl) {
    return new Promise(resolve => {
        imageToBase64(ImageUrl) // Image URL
            .then(
                (response) => {
                    // console.log('data:image/png;base64,' + response); // "iVBORw0KGgoAAAANSwCAIA..."
                    resolve({
                        status: true,
                        base64Img: 'data:image/png;base64,' + response
                    })
                }
            )
            .catch(
                (error) => {
                    resolve({
                        status: false,
                        base64Img: ''
                    })
                }
            )
    })
}

/* GET users listing. */
router.get('/', function(req, res, next) {
    //Load the docx file as a binary
    var content = fs
        .readFileSync(path.resolve(__dirname, 'input.docx'), 'binary');
    var zip = new PizZip(content);
    var doc;
    try {
        // doc = new Docxtemplater(zip);
        ImageToBase64Func('https://www.blogbaa.com/wp-content/uploads/2019/07/4DQpjUtzLUwmJZZPGToiukC0C8qzmEnb47FJ3mkT3ADY.jpg').then(resExec => {
            if (resExec.status) {
                doc = new Docxtemplater()
                    .attachModule(new ImageModule(imageOpts))
                    .loadZip(zip)
                    .setData({
                        first_name: 'John',
                        last_name: 'Doe',
                        phone: '0652455478',
                        description: 'New Website'
                    })
                    .render();

                var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

// buf is a nodejs buffer, you can either write it to a file or do anything else with it.
                fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);

                res.send('respond with a resource');
            }
        })

    } catch(error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }
    // try {
    //   // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    //   doc.render()
    // }
    // catch (error) {
    //   // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    //   errorHandler(error);
    // }
});

module.exports = router;
