var express = require('express');
var router = express.Router();

var PizZip = require('pizzip');
var Docxtemplater = require('docxtemplater');
var ImageModule = require('open-docxtemplater-image-module');
// var ImageModule = require("./es6");

var fs = require('fs');
var path = require('path');
const https = require("https");
const Stream = require("stream").Transform;
// const JSZip = require("jszip");

//Below the options that will be passed to ImageModule instance

var opts = {};
opts.getImage = function (tagValue, tagName) {
    console.log(tagValue, tagName);
    // tagValue is "https://docxtemplater.com/xt-pro-white.png" and tagName is "image"
    return new Promise(function (resolve, reject) {
        getHttpData(tagValue, function (err, data) {
            if (err) {
                return reject(err);
            }
            resolve(data);
        });
    });
};

//Pass the function that return image size
opts.getSize = function (img, tagValue, tagName) {
    console.log(tagValue, tagName);
    // img is the value that was returned by getImage
    // This is to force the width to 600px, but keep the same aspect ration
    const sizeOf = require("image-size");
    const sizeObj = sizeOf(img);
    console.log(sizeObj);
    const forceWidth = 600;
    const ratio = forceWidth / sizeObj.width;
    return [
        forceWidth,
        // calculate height taking into account aspect ratio
        Math.round(sizeObj.height * ratio),
    ];
};

const imageModule = new ImageModule(opts);

// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
function replaceErrors(key, value) {
    if (value instanceof Error) {
        return Object.getOwnPropertyNames(value).reduce(function (error, key) {
            error[key] = value[key];
            return error;
        }, {});
    }
    return value;
}

function errorHandler(error) {
    console.log(JSON.stringify({error: error}, replaceErrors));

    if (error.properties && error.properties.errors instanceof Array) {
        const errorMessages = error.properties.errors.map(function (error) {
            return error.properties.explanation;
        }).join("\n");
        console.log('errorMessages', errorMessages);
        // errorMessages is a humanly readable message looking like this :
        // 'The tag beginning with "foobar" is unopened'
    }
    throw error;
}

function getHttpData(url, callback) {
    https
        .request(url, function (response) {
            if (response.statusCode !== 200) {
                return callback(
                    new Error(
                        `Request to ${url} failed, status code: ${response.statusCode}`
                    )
                );
            }

            const data = new Stream();
            response.on("data", function (chunk) {
                data.push(chunk);
            });
            response.on("end", function () {
                callback(null, data.read());
            });
            response.on("error", function (e) {
                callback(e);
            });
        })
        .end();
}

router.get('/', function (req, res, next) {
    //Load the docx file as a binary
    var content = fs
        .readFileSync(path.resolve(__dirname, 'input.docx'), 'binary');
    var zip = new PizZip(content);
    var doc;
    try {
        doc = new Docxtemplater()
            .attachModule(imageModule)
            .loadZip(zip)
            .compile()

        doc.resolveData({
            first_name: 'John',
            last_name: 'Doe',
            phone: '0652455478',
            description: 'New Website',
            // image: 'https://docxtemplater.com/xt-pro-white.png'
        }).then(function (response) {
            console.log(response)
            console.log("data resolved");
            doc.render();
            // const buffer = doc
            //     .getZip()
            //     .generate({
            //         type: "nodebuffer",
            //         compression: "DEFLATE"
            //     });

            // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
            //     fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buffer);

            res.send('respond with a resource');
        }).catch(function (error) {
            console.log("An error occured", error);
        });
    } catch (error) {
        // Catch compilation errors (errors caused by the compilation of the template : misplaced tags)
        errorHandler(error);
    }
});

module.exports = router;
